#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtSql>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButtonTrabajos_clicked();

    void on_pushButtonPersonas_clicked();

private:
    void databaseConnect();
    void databaseInit();
    void databasePopulate();
    void databaseShow();

private:
    Ui::Dialog *ui;
    QSqlQueryModel *modelo;
    QSqlQuery * query;
    QSqlDatabase db;
};

#endif // DIALOG_H
