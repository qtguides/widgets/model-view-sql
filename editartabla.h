#ifndef EDITARTABLA_H
#define EDITARTABLA_H

#include <QDialog>
#include <QSqlTableModel>

namespace Ui {
class EditarTabla;
}

class EditarTabla : public QDialog
{
    Q_OBJECT

public:
    explicit EditarTabla(QString tabla, QWidget *parent = nullptr);
    ~EditarTabla();

private slots:
    void on_pushButtonInsertar_clicked();
    void on_pushButtonEliminar_clicked();

private:
    Ui::EditarTabla *ui;
    QSqlTableModel * modelo;
};

#endif // EDITARTABLA_H
