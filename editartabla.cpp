#include "editartabla.h"
#include "ui_editartabla.h"

EditarTabla::EditarTabla(QString tabla, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditarTabla)
{
    ui->setupUi(this);

    modelo = new QSqlTableModel(this);
    modelo->setTable(tabla);
    modelo->select();
    ui->tableView->setModel(modelo);
}

EditarTabla::~EditarTabla()
{
    delete ui;
}

void EditarTabla::on_pushButtonInsertar_clicked()
{
    int row = modelo->rowCount();
    modelo->insertRow(row);

    QModelIndex index = modelo->index(row, 0);
    ui->tableView->setCurrentIndex(index);
    ui->tableView->edit(index);
}

void EditarTabla::on_pushButtonEliminar_clicked()
{
    modelo->removeRow(ui->tableView->currentIndex().row());
    modelo->select(); // vuelve a ejecutar el SELECT de la tabla refrescando la vista
}

