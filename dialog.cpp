#include "dialog.h"
#include "ui_dialog.h"
#include "editartabla.h"
#include <QSqlDatabase>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    databaseConnect();

    QStringList list = db.tables(QSql::Tables);
    if(list.count() == 0) // Database ya inicializado
    {
        databaseInit();
        databasePopulate();
    }

    databaseShow();
}

Dialog::~Dialog()
{
    delete ui;
}


void Dialog::databaseConnect()
{
    const QString DRIVER("QSQLITE");

    if(QSqlDatabase::isDriverAvailable(DRIVER))
    {
        db = QSqlDatabase::addDatabase(DRIVER);
        db.setDatabaseName("db.sqlite");
        //db.setDatabaseName(":memory:");
        if(!db.open())
            qWarning() << __FUNCTION__ << " ERROR: " << db.lastError().text();
    }
    else
    {
        qWarning() << __FUNCTION__ << " ERROR: no driver " << DRIVER << " available";
    }
}

void Dialog::databaseInit()
{
    QSqlQuery query;

    if( !query.exec("CREATE TABLE trabajo (id INTEGER PRIMARY KEY, descripcion VARCHAR(50))"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }

    if( !query.exec("CREATE TABLE persona("
                        "id INTEGER PRIMARY KEY, "
                        "nombre VARCHAR(50), "
                        "edad INTEGER, "
                        "trabajoid INTEGER, "
                        "FOREIGN KEY(trabajoid) REFERENCES trabajo(id))"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }
}

void Dialog::databasePopulate()
{
    QSqlQuery query;

    if(    !query.exec("INSERT INTO trabajo(id, descripcion) VALUES(1, 'Peleador')")
        || !query.exec("INSERT INTO trabajo(id, descripcion) VALUES(2, 'Corredor')")
        || !query.exec("INSERT INTO trabajo(id, descripcion) VALUES(3, 'Actor')")
        || !query.exec("INSERT INTO trabajo(id, descripcion) VALUES(4, 'Personaje')")
        || !query.exec("INSERT INTO trabajo(id, descripcion) VALUES(5, 'Programador')"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }

    if(    !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Martin Karadagian', 69, 1)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Ramon Valdez',      64, 3)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Pierre Nodoyuna',   45, 2)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Dana Scully',       53, 4)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Jason Bourne',      48, 4)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Clark Kent',        34, 4)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Mel Gibson',        63, 3)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Jon Snow',          28, 4)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Linus Torvalds',    49, 5)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Michael Moore',     64, 3)")
        || !query.exec("INSERT INTO persona(nombre, edad, trabajoid) VALUES('Sancho Panza',      51, 4)"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }
}

void Dialog::databaseShow()
{
    modelo = new QSqlQueryModel();
    query = new QSqlQuery("SELECT persona.id as Id, nombre as Nombe, edad as Eddad, trabajo.id as EmpleoId, descripcion as Empleo "
                          "FROM persona, trabajo "
                          "WHERE persona.trabajoid = trabajo.id");
    modelo->setQuery(*query);
    ui->tableView->setModel(modelo);
}

void Dialog::on_pushButtonTrabajos_clicked()
{
    EditarTabla dialog("trabajo", this);
    dialog.exec();
    query->exec();
    modelo->setQuery(*query);
}

void Dialog::on_pushButtonPersonas_clicked()
{
    EditarTabla dialog("persona", this);
    dialog.exec();
    query->exec();
    modelo->setQuery(*query);
}
